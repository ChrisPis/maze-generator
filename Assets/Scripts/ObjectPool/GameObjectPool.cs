using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameObjectPool
{
    public GameObject prefab;
    public Transform parent;

    public Stack<GameObject> Pool { get; } = new Stack<GameObject>();

    public GameObjectPool(GameObject prefab, Transform parent)
    {
        this.prefab = prefab;
        this.parent = parent;
    }

    //Adds a new instance of the prefab to the pool
    private void GrowPool()
    {
        GameObject newObject = Object.Instantiate(prefab, parent);
        AddToPool(newObject);
    }

    //Adds an existing instance of the prefab to the pool
    public void AddToPool(GameObject newObject)
    {
        newObject.SetActive(false);
        Pool.Push(newObject);
    }

    //Gets an instance of the prefab from the pool
    //If the pool is empty, it creates a new instance
    public GameObject GetFromPool()
    {
        if (Pool.Count == 0)
            GrowPool();

        GameObject newObject = Pool.Pop();
        newObject.SetActive(true);

        return newObject;
    }

    public GameObject GetFromPool(Vector3 position, Vector3 scale, Quaternion rotation)
    {
        if (Pool.Count == 0)
            GrowPool();

        GameObject newObject = Pool.Pop();
        newObject.SetActive(true);
        newObject.transform.localPosition = position;
        newObject.transform.localScale = scale;
        newObject.transform.localRotation = rotation;

        return newObject;
    }
}
