using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Maze class used to pass the maze values to an event
public class MazeData
{
    public Vector3 center;
    public float width, height, scale;
    public MazeCell[,] cells;
    public MazeData(Vector3 center, float width, float height, float scale, MazeCell[,] cells)
    {
        this.center = center;
        this.width = width;
        this.height = height;
        this.scale = scale;
        this.cells = cells;
    }
}
