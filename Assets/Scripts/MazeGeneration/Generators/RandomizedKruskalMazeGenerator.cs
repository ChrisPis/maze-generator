using System;
using System.Collections.Generic;

public class RandomizedKruskalMazeGenerator : MazeGenerator
{
    private List<Edge> walls = new List<Edge>();
    private int[] parent;

    public RandomizedKruskalMazeGenerator(int width, int height) : base(width, height) { }

    public override MazeCell[,] GenerateMaze()
    {
        parent = new int[mazeWidth * mazeHeight];

        //Creates an array of sets for the maze cells
        for (int i = 0; i < parent.Length; i++)
        {
            parent[i] = i;
        }

        //Creates list of all walls
        foreach (MazeCell cell in mazeCells)
        {
            //North
            if (cell.y < mazeHeight - 1)
            {
                walls.Add(new Edge(cell, mazeCells[cell.x, cell.y + 1], (int)Direction.UP));
            }

            //East
            if (cell.x < mazeWidth - 1)
            {
                walls.Add(new Edge(cell, mazeCells[cell.x + 1, cell.y], (int)Direction.RIGHT));
            }
        }

        while (walls.Count != 0)
        {
            //Gets a random wall from the wall list
            int currentIndex = random.Next(0, walls.Count);
            Edge currentWall = walls[currentIndex];
            MazeCell cell1 = currentWall.cell1,
                cell2 = currentWall.cell2;

            //Finds the 2 data set ints of the wall's seperated cells
            int irep = Find(CoordToIndex(cell1.x, cell1.y));
            int jrep = Find(CoordToIndex(cell2.x, cell2.y));

            //If cells are from a different set, get rid of the seperating wall and join the set
            if (irep != jrep)
            {
                //Depending on the diretion of the edge, it removes the corresponding walls of both cells
                switch (currentWall.direction)
                {
                    case (int)Direction.UP:
                        cell1.topWall = false;
                        cell2.bottomWall = false;
                        break;
                    case (int)Direction.RIGHT:
                        cell1.rightWall = false;
                        cell2.leftWall = false;
                        break;
                }

                Union(irep, jrep);
            }

            walls.RemoveAt(currentIndex);
        }

        return mazeCells;
    }

    //Finds parent int
    private int Find(int i)
    {
        if (parent[i] == i)
        {
            return i;
        }
        else
        {
            int result = Find(parent[i]);

            //Sets the result as new current parent to make finding faster for future finds
            parent[i] = result;
            return result;
        }
    }

    //Unites the 2 disjoint data sets
    private void Union(int i, int j)
    {
        int iRep = Find(i),
            jRep = Find(j);

        parent[iRep] = jRep;
    }

    private int CoordToIndex(int x, int y)
    {
        return x + y * mazeWidth;
    }
}
