using System.Collections.Generic;

public class WilsonsAlgorithmMazeGenerator : MazeGenerator
{
    private int[,] currentWalk;

    public WilsonsAlgorithmMazeGenerator(int width, int height) : base(width, height) { }

    public override MazeCell[,] GenerateMaze()
    {
        //Creates array for the random walk and sets a begin point
        currentWalk = new int[mazeWidth, mazeHeight];
        mazeCells[0, 0].visited = true;

        //For every unvisited cell, it will start a random walk
        for (int x = 0; x < mazeWidth; x++)
        {
            for (int y = 0; y < mazeHeight; y++)
            {
                if (!mazeCells[x, y].visited)
                {
                    LoopErasedRandomWalk(x, y);
                }
            }
        }
        return mazeCells;
    }

    public void LoopErasedRandomWalk(int x, int y)
    {
        int currentX = x, currentY = y;
        int prevX, prevY;

        //Sets the random starting direction of the walk
        currentWalk[x, y] = randomWalkDirection(x, y);

        while (true)
        {
            prevX = currentX;
            prevY = currentY;

            //Goes to next point depending on direction
            int[] step = StepDirection(currentWalk[currentX, currentY]);
            currentX += step[0];
            currentY += step[1];

            //When the walk finds a visited maze cell, it adds the walk path to the maze cells and breaks the while loop
            if (mazeCells[currentX, currentY].visited)
            {
                AddWalkToCells(x, y, prevX, prevY);
                break;
            }


            //When the walk finds an already walked on point, it removes the created loop from the walk and continues
            //walking from the collision point
            if (currentWalk[currentX, currentY] != (int)Direction.NONE)
                EraseWalk(currentX, currentY, prevX, prevY);

            //Sets a new random direction for the next walk
            int randomDir = randomWalkDirection(currentX, currentY);
            currentWalk[currentX, currentY] = randomDir;
        }
    }

    private void AddWalkToCells(int x1, int y1, int x2, int y2)
    {
        int currentX = x1, currentY = y1;

        //Loops through all walked points, until all directions are removed from the walk
        while (currentWalk[currentX, currentY] != (int)Direction.NONE)
        {
            int prevX = currentX, prevY = currentY;
            mazeCells[currentX, currentY].visited = true;

            //Removes walls depending on the direction of the current point
            switch (currentWalk[currentX, currentY])
            {
                case (int)Direction.UP:
                    mazeCells[currentX, currentY + 1].bottomWall = false;
                    mazeCells[currentX, currentY].topWall = false;
                    currentY++;
                    break;
                case (int)Direction.RIGHT:
                    mazeCells[currentX + 1, currentY].leftWall = false;
                    mazeCells[currentX, currentY].rightWall = false;
                    currentX++;
                    break;
                case (int)Direction.DOWN:
                    mazeCells[currentX, currentY - 1].topWall = false;
                    mazeCells[currentX, currentY].bottomWall = false;
                    currentY--;
                    break;
                case (int)Direction.LEFT:
                    mazeCells[currentX - 1, currentY].rightWall = false;
                    mazeCells[currentX, currentY].leftWall = false;
                    currentX--;
                    break;
            }
            currentWalk[prevX, prevY] = (int)Direction.NONE;
        }
    }

    private void EraseWalk(int x1, int y1, int x2, int y2)
    {
        int currentX = x1, currentY = y1;

        //Loops through all walked points between the 1 and 2 coordinate and removes the directions
        while (currentWalk[currentX, currentY] != (int)Direction.NONE)
        {
            int prevX = currentX, prevY = currentY;

            //Goes to next point depending on direction
            int[] step = StepDirection(currentWalk[currentX, currentY]);
            currentX += step[0];
            currentY += step[1];

            currentWalk[prevX, prevY] = (int)Direction.NONE;
        }
    }

    //Array index 0 is x, and index 1 is y
    private int[] StepDirection(int direction)
    {
        int[] step = { 0, 0 };

        //Gets coordinate direction depending on direction
        switch (direction)
        {
            case (int)Direction.UP:
                step[1] = 1;
                break;
            case (int)Direction.RIGHT:
                step[0] = 1;
                break;
            case (int)Direction.DOWN:
                step[1] = -1;
                break;
            case (int)Direction.LEFT:
                step[0] = -1;
                break;
        }

        return step;
    }

    //Chooses a random possible direction for the walk to go in
    private int randomWalkDirection(int x, int y)
    {
        List<int> possibleDirections = new List<int>();

        //South
        if (y > 0 && currentWalk[x, y - 1] != (int)Direction.UP)
        {
            possibleDirections.Add((int)Direction.DOWN);
        }

        //East
        if (x < mazeWidth - 1 && currentWalk[x + 1, y] != (int)Direction.LEFT)
        {
            possibleDirections.Add((int)Direction.RIGHT);
        }

        //North
        if (y < mazeHeight - 1 && currentWalk[x, y + 1] != (int)Direction.DOWN)
        {
            possibleDirections.Add((int)Direction.UP);
        }

        //West
        if (x > 0 && currentWalk[x - 1, y] != (int)Direction.RIGHT)
        {
            possibleDirections.Add((int)Direction.LEFT);
        }

        //Chooses a random direction out of the possible directions
        if (possibleDirections.Count != 0)
        {
            int randomIndex = random.Next(0, possibleDirections.Count);
            return possibleDirections[randomIndex];
        }
        else
            //Returns NONE when no direction is possible
            return (int)Direction.NONE;
    }
}
