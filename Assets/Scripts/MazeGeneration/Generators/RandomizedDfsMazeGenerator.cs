using System;
using System.Collections.Generic;

public class RandomizedDfsMazeGenerator : MazeGenerator
{
    private Stack<MazeCell> visitedCells = new Stack<MazeCell>();

    public RandomizedDfsMazeGenerator(int width, int height) : base(width, height) { }

    public override MazeCell[,] GenerateMaze()
    {
        MazeCell currentCell = mazeCells[0, 0];
        visitedCells.Push(mazeCells[0, 0]);

        while (visitedCells.Count != 0)
        {
            currentCell.visited = true;

            //Gets random direction to move into
            int randomDir = randomDirection(currentCell.x, currentCell.y);


            //Checks for each direction which walls should be open and closed
            //Also sets the new current cell
            switch (randomDir)
            {
                case (int)Direction.UP:
                    currentCell.topWall = false;
                    currentCell = mazeCells[currentCell.x, currentCell.y + 1];
                    currentCell.bottomWall = false;
                    visitedCells.Push(currentCell);
                    break;
                case (int)Direction.RIGHT:
                    currentCell.rightWall = false;
                    currentCell = mazeCells[currentCell.x + 1, currentCell.y];
                    currentCell.leftWall = false;
                    visitedCells.Push(currentCell);
                    break;
                case (int)Direction.DOWN:
                    currentCell.bottomWall = false;
                    currentCell = mazeCells[currentCell.x, currentCell.y - 1];
                    currentCell.topWall = false;
                    visitedCells.Push(currentCell);
                    break;
                case (int)Direction.LEFT:
                    currentCell.leftWall = false;
                    currentCell = mazeCells[currentCell.x - 1, currentCell.y];
                    currentCell.rightWall = false;
                    visitedCells.Push(currentCell);
                    break;
                case (int)Direction.NONE:
                    //If dead end, pop stack
                    currentCell = visitedCells.Pop();
                    break;
            }
        }
        return mazeCells;
    }

    //Chooses a random possible direction for the cell to go in
    private int randomDirection(int xPos, int yPos)
    {
        List<int> possibleDirections = new List<int>();

        //South
        if (yPos > 0 && !CheckIfVisited(xPos, yPos - 1))
        {
            possibleDirections.Add((int)Direction.DOWN);
        }

        //East
        if (xPos < mazeWidth - 1 && !CheckIfVisited(xPos + 1, yPos))
        {
            possibleDirections.Add((int)Direction.RIGHT);
        }

        //North
        if (yPos < mazeHeight - 1 && !CheckIfVisited(xPos, yPos + 1))
        {
            possibleDirections.Add((int)Direction.UP);
        }

        //West
        if (xPos > 0 && !CheckIfVisited(xPos - 1, yPos))
        {
            possibleDirections.Add((int)Direction.LEFT);
        }

        //Chooses a random direction out of the possible directions
        if (possibleDirections.Count != 0)
        {
            int randomIndex = random.Next(0, possibleDirections.Count);
            return possibleDirections[randomIndex];
        }
        else
            //Returns NONE when no direction is possible
            return (int)Direction.NONE;
    }
}