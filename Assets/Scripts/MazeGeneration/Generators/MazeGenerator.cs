using System;

public abstract class MazeGenerator
{
    //Used for defining which direction the cell must go
    public enum Direction
    {
        NONE,
        UP,
        RIGHT,
        DOWN,
        LEFT
    }

    public int mazeWidth, mazeHeight;

    protected MazeCell[,] mazeCells;
    protected Random random = new Random();

    public MazeGenerator(int width, int height)
    {
        InitializeMaze(width, height);
    }

    public abstract MazeCell[,] GenerateMaze();

    public void InitializeMaze(int width, int height)
    {
        mazeWidth = width;
        mazeHeight = height;
        mazeCells = new MazeCell[width, height];

        //Initialize maze
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                mazeCells[x, y] = new MazeCell(x, y);
            }
        }
    }

    protected bool CheckIfVisited(int xPos, int yPos)
    {
        if (mazeCells[xPos, yPos] != null && mazeCells[xPos, yPos].visited)
        {
            return true;
        }
        else
            return false;
    }

    public class Edge
    {
        public MazeCell cell1;
        public MazeCell cell2;
        public int direction;

        public Edge(MazeCell cell1, MazeCell cell2, int direction)
        {
            this.cell1 = cell1;
            this.cell2 = cell2;
            this.direction = direction;
        }
    }
}
