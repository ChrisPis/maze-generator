
public class MazeCell
{
    public bool topWall, rightWall, bottomWall, leftWall;
    public bool visited;
    public int x, y;

    public MazeCell(int x, int y)
    {
        this.x = x;
        this.y = y;

        visited = false;
        topWall = true;
        rightWall = true;
        bottomWall = true;
        leftWall = true;
    }
}
