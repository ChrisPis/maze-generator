using System;
using System.Collections.Generic;

public class RandomizedPrimMazeGenerator : MazeGenerator
{
    private List<Edge> walls = new List<Edge>();

    public RandomizedPrimMazeGenerator(int width, int height) : base(width, height) { }

    public override MazeCell[,] GenerateMaze()
    {
        mazeCells[0, 0].visited = true;
        AddWalls(mazeCells[0, 0]);

        while (walls.Count != 0)
        {
            int randomIndex = random.Next(0, walls.Count);
            Edge currentEdge = walls[randomIndex];

            //Unvisited can only be cell2, since when creating an edge,
            //cell1 is always filled in first. And to add walls, the first cell must be visited
            MazeCell visitedCell = mazeCells[currentEdge.cell1.x, currentEdge.cell1.y];
            MazeCell newCell = mazeCells[currentEdge.cell2.x, currentEdge.cell2.y];

            if (!newCell.visited)
            {
                AddWalls(newCell);
                newCell.visited = true;

                //Depending on the diretion of the edge, it removes the corresponding walls of both cells
                switch (currentEdge.direction)
                {
                    case (int)Direction.UP:
                        visitedCell.topWall = false;
                        newCell.bottomWall = false;
                        break;
                    case (int)Direction.RIGHT:
                        visitedCell.rightWall = false;
                        newCell.leftWall = false;
                        break;
                    case (int)Direction.DOWN:
                        visitedCell.bottomWall = false;
                        newCell.topWall = false;
                        break;
                    case (int)Direction.LEFT:
                        visitedCell.leftWall = false;
                        newCell.rightWall = false;
                        break;
                }
            }

            walls.Remove(currentEdge);
        }

        return mazeCells;
    }

    //Evaluates which edges should be added to the walls list when visiting a new cell
    private void AddWalls(MazeCell cell)
    {
        //North
        if (cell.y < mazeHeight - 1 && !CheckIfVisited(cell.x, cell.y + 1))
        {
            walls.Add(new Edge(cell, mazeCells[cell.x, cell.y + 1], (int)Direction.UP));
        }

        //East
        if (cell.x < mazeWidth - 1 && !CheckIfVisited(cell.x + 1, cell.y))
        {
            walls.Add(new Edge(cell, mazeCells[cell.x + 1, cell.y], (int)Direction.RIGHT));
        }

        //South
        if (cell.y > 0 && !CheckIfVisited(cell.x, cell.y - 1))
        {
            walls.Add(new Edge(cell, mazeCells[cell.x, cell.y - 1], (int)Direction.DOWN));
        }

        //West
        if (cell.x > 0 && !CheckIfVisited(cell.x - 1, cell.y))
        {
            walls.Add(new Edge(cell, mazeCells[cell.x - 1, cell.y], (int)Direction.LEFT));
        }
    }
}
