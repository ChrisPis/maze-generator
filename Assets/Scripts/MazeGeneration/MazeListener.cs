using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeListener : MonoBehaviour
{
    enum Generators
    {
        dfsBacktracking,
        randomizedPrim,
        randomizedKruskal,
        wilsonsAlgorithm
    }

    private MazeController mazeController;

    private void Start()
    {
        mazeController = GetComponent<MazeController>();
    }

    public void OnSelectedGeneratorChanged(int i)
    {
        //Evaluates which maze type should be set for the controller
        switch (i)
        {
            case (int)Generators.dfsBacktracking:
                mazeController.mazeGenerator = new RandomizedDfsMazeGenerator(0, 0);
                break;
            case (int)Generators.randomizedPrim:
                mazeController.mazeGenerator = new RandomizedPrimMazeGenerator(0, 0);
                break;
            case (int)Generators.randomizedKruskal:
                mazeController.mazeGenerator = new RandomizedKruskalMazeGenerator(0, 0);
                break;
            case (int)Generators.wilsonsAlgorithm:
                mazeController.mazeGenerator = new WilsonsAlgorithmMazeGenerator(0, 0);
                break;
        }
    }
}
