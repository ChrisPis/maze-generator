using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MazeController : MonoBehaviour
{
    public int width = 10;
    public int height = 10;

    public float scale = 3;

    [SerializeField] private GameObject wall, floor;

    public UnityEvent<MazeData> onMazeGenerated;

    [HideInInspector] public MazeGenerator mazeGenerator;
    private List<GameObject> activeWalls = new List<GameObject>();
    private GameObjectPool wallsPool;
    private GameObject currentFloor;

    // Start is called before the first frame update
    void Start()
    {
        wallsPool = new GameObjectPool(wall, transform);
        mazeGenerator = new RandomizedDfsMazeGenerator(width, height);
        CreateMaze();
    }

    public void CreateMaze()
    {
        //Deletes previous maze, if there was one
        DeleteMaze();

        mazeGenerator.InitializeMaze(width, height);

        MazeCell[,] mazeCells = mazeGenerator.GenerateMaze();

        //Instantiates a wall for every maze cell where needed
        //If the cell is not on the far left or bottom, it will only place walls to the right or top of the cell
        //This prevents duplicate walls
        for (int x = 0; x < mazeGenerator.mazeWidth; x++)
        {
            for (int y = 0; y < mazeGenerator.mazeHeight; y++)
            {
                if (x == 0)
                {
                    activeWalls.Add(wallsPool.GetFromPool(new Vector3(x * scale - scale / 2, 0, y * scale), new Vector3(1, 1, scale), Quaternion.Euler(0, 0, 0)));
                }

                if (y == 0)
                {
                    activeWalls.Add(wallsPool.GetFromPool(new Vector3(x * scale, 0, y * scale - scale / 2), new Vector3(1, 1, scale), Quaternion.Euler(0, 90, 0)));
                }

                if (mazeCells[x, y].rightWall)
                {
                    activeWalls.Add(wallsPool.GetFromPool(new Vector3(x * scale + scale / 2, 0, y * scale), new Vector3(1, 1, scale), Quaternion.Euler(0, 0, 0)));
                }

                if (mazeCells[x, y].topWall)
                {
                    activeWalls.Add(wallsPool.GetFromPool(new Vector3(x * scale, 0, y * scale + scale / 2), new Vector3(1, 1, scale), Quaternion.Euler(0, 90, 0)));
                }
            }
        }

        Vector3 center = new Vector3((width * scale) / 2 - scale / 2, transform.position.y, (height * scale) / 2 - scale / 2);

        //Adds a floor to the maze
        Destroy(currentFloor);
        currentFloor = Instantiate(floor, center + Vector3.down, Quaternion.Euler(0, 0, 0), transform);
        currentFloor.transform.localScale = new Vector3(width * scale, 1, height * scale);

        //Invokes unity event on generation
        onMazeGenerated.Invoke(new MazeData(center, width, height, scale, mazeCells));
    }

    private void DeleteMaze()
    {
        foreach (GameObject wall in activeWalls)
        {
            wallsPool.AddToPool(wall);
        }

        activeWalls.Clear();
    }
}
