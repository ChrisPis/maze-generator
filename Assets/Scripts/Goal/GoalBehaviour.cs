using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GoalBehaviour : MonoBehaviour
{
    public string playerTag = "Player";

    public UnityEvent<Vector3> onGoalReached;

    private bool goalReached;

    private void Start()
    {
        ResetGoal();
    }

    private void OnTriggerEnter(Collider other)
    {
        //Activates goal event when player reaches the trigger box
        if (!goalReached && other.CompareTag(playerTag))
        {
            goalReached = true;
            onGoalReached.Invoke(transform.position);
        }
    }

    public void ResetGoal()
    {
        goalReached = false;
    }
}
