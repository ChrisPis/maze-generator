using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalListener : MonoBehaviour
{
    public void OnMazeGenerated(MazeData maze)
    {
        //Sets position on top right of maze
        transform.position = maze.center + new Vector3(maze.width * maze.scale / 2 - maze.scale / 2, 0, maze.height * maze.scale / 2 - maze.scale / 2);
    }
}
