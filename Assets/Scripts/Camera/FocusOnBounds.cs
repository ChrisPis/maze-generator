using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FocusOnBounds : MonoBehaviour
{
    public Camera camera;

    public void SetFocusBounds(Vector3 center, float width, float height)
    {
        Vector3 newPosition = center;

        //Assumes the height of the camera frustum to be the height of the focussed object
        float frustumHeight = height;

        //If the width of the maze, relative to the aspect ratio of the screen,
        //is bigger than the height of the maze, the frustum height is calculated using the frustum width
        if (width / Screen.width >= height / Screen.height)      
            frustumHeight = width / camera.aspect;

        float distance = frustumHeight * 0.5f / Mathf.Tan(camera.fieldOfView * 0.5f * Mathf.Deg2Rad);

        newPosition.y += distance;
        transform.position = newPosition;
    }
}
