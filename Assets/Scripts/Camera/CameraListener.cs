using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraListener : MonoBehaviour
{
    public float extraMazeView;

    private FocusOnBounds focusBounds;

    private void Start()
    {
        focusBounds = GetComponent<FocusOnBounds>();
    }

    public void OnMazeBoundsChanged(MazeData maze)
    {
        focusBounds.SetFocusBounds(maze.center, maze.width * maze.scale + extraMazeView, maze.height * maze.scale + extraMazeView);
    }
}
