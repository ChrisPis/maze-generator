using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float maxSpeed = 10, accelerationSpeed = 1, friction = 0.98f;

    private Rigidbody rb;
    private Vector3 velocity;
    private int hAxis, vAxis;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        MovementInput();
    }

    private void FixedUpdate()
    {
        Movement();
    }

    private void MovementInput()
    {
        if (Input.GetKey(KeyCode.A))
            hAxis = -1;
        if (Input.GetKey(KeyCode.D))
            hAxis = 1;
        if (Input.GetKey(KeyCode.S))
            vAxis = -1;
        if (Input.GetKey(KeyCode.W))
            vAxis = 1;
    }

    private void Movement()
    {
        velocity = rb.velocity;

        //Prevents velocity from reaching impossibly low values
        if (Math.Abs(velocity.x) < 0.01f)
            velocity.x = 0;
        if (Math.Abs(velocity.z) < 0.01f)
            velocity.z = 0;

        Vector3 direction = new Vector3(hAxis, 0, vAxis).normalized;
        Vector3 acceleration = direction * accelerationSpeed;

        velocity += acceleration;
        velocity *= friction;

        //Sets max speed
        if (velocity.magnitude > maxSpeed)
            velocity = velocity.normalized * maxSpeed;

        rb.velocity = velocity;

        //Resets movement input
        hAxis = 0;
        vAxis = 0;
    }
}
