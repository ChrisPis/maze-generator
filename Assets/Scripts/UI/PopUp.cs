using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUp : MonoBehaviour
{
    public float openTime, closeTime;
    public LeanTweenType easeEffect;

    public void OpenPopUp()
    {
        transform.localScale = Vector3.zero;
        gameObject.SetActive(true);
        LeanTween.scale(gameObject, Vector3.one, closeTime).setEase(easeEffect);
    }

    public void ClosePopUp()
    {
        LeanTween.scale(gameObject, Vector3.zero, closeTime).setEase(easeEffect).setOnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }
}
