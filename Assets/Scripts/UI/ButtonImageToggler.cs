using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonImageToggler : MonoBehaviour
{
    public Image image;

    public Sprite sprite1, sprite2;

    private Button button;
    // Start is called before the first frame update
    void Start()
    {
        button = GetComponent<Button>();
        button.onClick.AddListener(ToggleImage);
    }

    private void ToggleImage()
    {
        if (image.sprite == sprite1)
        {
            image.sprite = sprite2;
        }
        else
            image.sprite = sprite1;
    }
}
