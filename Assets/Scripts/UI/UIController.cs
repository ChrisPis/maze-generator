using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public MazeController mazeGenerator;

    public InputField scaleField, widthField, heightField;

    public void SetMazeWidth()
    {
        if (widthField.text.Length != 0)
        {
            int width = int.Parse(widthField.text);
            if (width > 0)
                mazeGenerator.width = width;
        }
    }

    public void SetMazeHeight()
    {
        if (heightField.text.Length != 0)
        {
            int height = int.Parse(heightField.text);
            if (height > 0)
                mazeGenerator.height = height;
        }
    }

    public void SetMazeScale()
    {
        if (scaleField.text.Length != 0)
        {
            int scale = int.Parse(scaleField.text);
            if (scale > 0)
                mazeGenerator.scale = scale;
        }
    }
}
