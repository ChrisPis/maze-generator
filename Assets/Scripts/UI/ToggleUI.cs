using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleUI : MonoBehaviour
{
    public float startX, endX;
    public float moveTime;

    public LeanTweenType easeEffect;

    private bool open = true;

    private RectTransform rectTransform;

    // Start is called before the first frame update
    void Start()
    {
        rectTransform = GetComponent<RectTransform>();
    }

    public void ToggleOpenAndClosed()
    {
        if (open)
        {
            open = false;
            MoveX(endX);
        }
        else
        {
            open = true;
            MoveX(startX);
        }
    }

    public void MoveX(float goal)
    {
        LeanTween.cancel(rectTransform);
        LeanTween.moveX(rectTransform, goal, moveTime).setEase(easeEffect);
    }
}
